require 'spec_helper'

describe SelectAttributesController do
  routes { BluewhaleSocial::Engine.routes }

  before(:all) { SelectAttribute.create(name: 'aSelect', options: { 1 => 'option1', 2 => 'option2'})}

  describe 'should get options values for an specifc select name' do
     let(:get_options_request) { get :get_options_by_name, name: 'aSelect'}

     it { JSON.parse(get_options_request.body)['1'].should eq 'option1' }

  end
end