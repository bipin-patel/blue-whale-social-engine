require 'spec_helper'

describe RegisterController do
  routes { BluewhaleSocial::Engine.routes }

  context 'Registration' do
    let(:register_json) { {user: json(:user) }}
    let(:register_request) { post :create, register_json }

    it do
      expect { register_request }.to change(User, :count).by 1
    end
    it do
      JSON.parse(register_request.body)['user']['authentication_token'].should_not eq nil
    end
  end

end