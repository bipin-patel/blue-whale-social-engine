require 'spec_helper'

describe User do
 
  context 'user_has_role' do
  	it { puts build(:user_with_roles).user_type.should eq("Role 1") }
  end

  context 'save user with role' do
    it { puts build(:user_with_roles).user_type.should eq("Role 1") }
  end

  # context 'user_has_a_user_type' do
 # 	it { build(:user).user_type.should eq("Role 1") }
 # end
end