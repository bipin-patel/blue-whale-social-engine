require 'spec_helper'

describe User do

  before(:each) { create(:user, username: 'a_username', email: 'an_email@test.com') }

  context 'invalid user' do
    it { build(:user, username: 'a_username_other', email: 'An_Email@test.com').should_not be_valid }
    it { build(:user, username: 'A_username', email: 'an_email_other@test.com').should_not be_valid }
    it { build(:user, username: 'A_username', email: 'An_Email@test.com').should_not be_valid }


  end

  context 'valid user' do
    it { build(:user, username: 'A_username_Other', email: 'An_Email_Other@test.com',:roles => [build(:role)]).should be_valid }

  end


end