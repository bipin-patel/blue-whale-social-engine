module RegisterMacros

  def register_user
    @user = User.new(get_user_json)
    @user.authentication_token = User.digest(Time.now.to_s)
    @user.roles << build(:role)
    @user.save
    @user.authentication_token
  end

  


  def get_user_json
    json(:registered_spec_user)
  end
end