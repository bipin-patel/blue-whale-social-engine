module BluewhaleSocial
  class Engine < ::Rails::Engine
  	#isolate_namespace BluewhaleSocial
    config.to_prepare do
      Dir.glob(Rails.root + "app/decorators/**/*_decorator*.rb").each do |c|
        require_dependency(c)
      end
    end

    initializer "model_core.factories", :after => "factory_girl.set_factory_paths" do
      FactoryGirl.definition_file_paths << File.expand_path('../../../spec/factories', __FILE__) if defined?(FactoryGirl)
      FactoryGirl.definition_file_paths << File.expand_path('../../../spec/support', __FILE__) if defined?(FactoryGirl)
    end

  end
end
 