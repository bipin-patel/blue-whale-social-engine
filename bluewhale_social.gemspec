$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "bluewhale_social/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "bluewhale_social"
  s.version     = BluewhaleSocial::VERSION
  s.authors     = ["Mike Jaffe"]
  s.email       = ["mike@bluewhaleapps.com"]
  s.homepage    = "https://github.com/bluewhaleinc/"
  s.summary     = "Provides a starting framework for social json api"
  s.description = "Provides a starting framework for social json api"

  s.files = Dir["{app,config,db,lib}/**/*", "spec/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.0"
  s.add_dependency "paperclip", "~> 3.5.2"
  s.add_dependency "aws-sdk"
  s.add_dependency 'kaminari'
  s.add_dependency 'koala'
  s.add_development_dependency "pg"

end
