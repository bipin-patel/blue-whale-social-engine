class Follower < ActiveRecord::Base
	belongs_to :user
	belongs_to :followed_user , :class_name => "User", :foreign_key => "following_id"

	validates :user_id, :uniqueness => { :scope => :following_id, :message => "is already following" }
	validates :user_id, :presence => true
	validates :following_id, :presence => true	

	default_scope { where(:blocked => false) }
end
