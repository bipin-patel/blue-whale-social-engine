class Category < ActiveRecord::Base
	validates_uniqueness_of :name ,:scope => :object_type,:on => :create
	has_many :post_categories, :dependent => :destroy
	has_many :user_categories, :dependent => :destroy
end
