class Favorite < ActiveRecord::Base
	validates_uniqueness_of :object_type_id, :scope => [:object_type,:user_id]
end
