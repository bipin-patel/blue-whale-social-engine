class User < ActiveRecord::Base
	
	validates_presence_of :email,:username
	validates :email, uniqueness: { case_sensitive: false }
	validates :username, uniqueness: { case_sensitive: false }
	validates :facebook_id, :uniqueness => true, :allow_blank => true
	validates :twitter_id, :uniqueness => true, :allow_blank => true
	validates :username, :exclusion => { :in => %w(admin superuser) }
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
	validates :username, :length => { :minimum => 6 }
	validates :password, :length => { :minimum => 6 },:if => :password_required?
	validates :username, :format => { :with => /\A^[a-zA-Z0-9\-\-_\.]+$+\Z/i ,:message => "can only contain letters, numbers, dashes, dots and underscores."}
	validates :password, :confirmation => true ,:if => :password_required?
	validates :password_confirmation, :presence =>true, :if => :password_required?
	

  has_attached_file :avatar,   :storage => :s3,
   :s3_credentials => YAML.load_file('config/s3.yml'),
  :styles => {  
		:avatar_small2x => ["84x84",:jpg],
		:avatar_small => ["42x42",:jpg],
		:avatar_medium2x => ["150x150",:jpg],
		:avatar_medium => ["75x75",:jpg],
		:avatar_large2x => ["218x218",:jpg],
		:avatar_large => ["109x109",:jpg]
    }, 
  :path => ":class/:attachment/:id/:basename-:style.:extension",
 :default_url => ":class/missing.png",
  :s3_permissions => "public-read"


 validates_attachment_size :avatar, :less_than => 15.megabytes
 validates_attachment_content_type :avatar, :content_type => ['image/jpg','image/jpeg','image/png'], :message => " must be a JPG or PNG."

 before_create :pass_gen 
 after_create :create_friends
 after_save :update_roles
 default_scope { where(:is_deleted => false) }

 attr_accessor :authenticated,:roles_assignment

 
 has_many :followers, :class_name => "Follower",:foreign_key => "following_id"
 has_many :follows, :class_name => "Follower",:foreign_key => "user_id", :dependent => :destroy
 has_many :posts, :dependent => :destroy
 has_many :post_subscribers, :dependent => :destroy
 has_many :likes, :dependent => :destroy
 has_many :invites, :dependent => :destroy
 has_many :groups,   class_name: 'Group', foreign_key: "object_owner_id", :dependent => :destroy
	has_many :user_categories, :dependent => :destroy
	has_many :categories, :through => :user_categories 
 belongs_to :user_groups, :dependent => :destroy
 has_many :users_roles, :dependent => :destroy
 has_many :roles,:through => :users_roles
 has_many :post_favorites,  -> { where object_type: "post" }, :class_name => "Favorite" , :dependent => :destroy
 has_many :user_favorites,  -> { where object_type: "user" }, :class_name => "Favorite", :foreign_key => "user_id", :dependent => :destroy

 

	##
	# Updates roles, if necissary
	def update_roles
		if self.roles_assignment.present?
			self.users_roles.delete_all
			self.roles_assignment.each { |r| UsersRole.create(:role_id => r,:user_id => self.id) }
		end
	end

	##
	# Returns the first user role and assumes its the primary user_type
	def user_type
		self.roles[0].name if self.roles.present?
	end

	def associate_categories(items)
		self.user_categories.destroy_all
		items.each do |c|
			cat = Category.where(:name => c, :object_type => "user").first
			if cat.present?
				self.user_categories.create({:category_id => cat.id})
			else 
				cat = Category.new({:name => c, :object_type => "user"})
					cat.save
					self.user_categories.create({:category_id => cat.id})

			end
		end
		return true
	end


 	def pending_invites
 			User
 			.joins(:invites)
 			.joins("LEFT JOIN followers on followers.following_id = invites.user_id and followers.user_id = invites.invited_user_id")
 			.where(["invites.invited_user_id = ?",self.id]).where("followers.id is null").order("users.email")
 	end

	def has_liked(user_id)
		Like.exists?(["user_id = ? and object_type_id = ? and object_type = 'user'",user_id,self.id])
	end

	def is_invited(user_id)
		Invite.exists?(["user_id = ? and (invited_email = ? or invited_id = ? or invited_user_id = ?)",user_id,self.email,self.facebook_id,self.id])
	end

 	def is_following(id)
 		Follower.exists?(["user_id = ? and following_id = ?",self.id,id])
 	end

 	def full_name
 	  "#{self.first_name} #{self.last_name}"
 	end

	def password_required?
        (self.new_record?  || self.password_confirmation) && !self.facebook_id 
      end

      ##
      # Looks for invitations and relates them as friends
      def create_friends
      	invites = Invite.where(["invited_email = ? or invited_id = ?",self.email,self.facebook_id])
      	invites.each do |i|
      		Follower.create(:user_id => self.id,:following_id => i.user_id)
      		Follower.create(:user_id => i.user_id,:following_id => self.id )
      	end
      end

  #     def auto_create_facebook_friends
		 
		# graph = Koala::Facebook::API.new(self.oauth_token)
		# friends = graph.get_connections("me", "friends")
		# if !friends.blank?
		# 	friends.each do |f|
		# 		link_user = user.where(:facebook_id => f["id"]).first
		# 		if link_user.present?
		#       		Follower.create(:user_id => self.id,:following_id => link_user.id)
		#       		Follower.create(:user_id => link_user,:following_id => self.id )
		# 		end
		# 	end
		# end
  #     end
 
 	def self.authenticate_with_facebook(user_params)
			if user_params[:avatar].present?
				user_params[:avatar] = User.decode_avatar(user_params[:avatar])
			end
 			user = User.where(["facebook_id = ?",user_params[:facebook_id]]).first
 			if user.present?
 				user.update_attribute(:authentication_token , User.digest(Time.now.to_s) )  if user.authentication_token.blank?
 				user.update_attribute(:oauth_token,user_params[:oauth_token]) if user_params[:oauth_token].present?
 				user.update_attribute(:device_id,user_params[:device_id]) if user_params[:device_id].present?
				user.authenticated = true
 				return user
 			else
 				# See if email for the the facebook user exists, if so just merge the account with facebook
 				user = User.where(:email => user_params[:email].downcase).first
 				if user.present?
 					user.update_attribute(:facebook_id, user_params[:facebook_id])
	 				user.update_attribute(:authentication_token , User.digest(Time.now.to_s) )  if user.authentication_token.blank?
	 				user.update_attribute(:oauth_token,user_params[:oauth_token]) if user_params[:oauth_token].present?
	 				user.update_attribute(:device_id,user_params[:device_id]) if user_params[:device_id].present?
					user.authenticated = true
	 				return user
 				end

 				# couldn't find facebook id or email, so create the account.
 				user = User.new(user_params)
 				user.username = "#{user.last_name.gsub(' ','')}#{user.facebook_id}"
 				user.password = User.digest(Time.now.to_s)
 				user.authentication_token = User.digest(Time.now.to_s)
 				if user.valid?
 					user.save
 					user.authenticated = true
 				end
 				return user
 			end
 		 
 	end

 	def self.decode_avatar(string64) 
		  photo_data = StringIO.new(Base64.decode64(string64))
	      photo_data.class.class_eval { attr_accessor :original_filename, :content_type }
	      photo_data.original_filename = "avatar.jpg"
	      photo_data.content_type = "image/jpg"
	      return photo_data
 	end

	def self.authenticate(username,password,facebook_id=nil)

		if facebook_id.nil?
			user = User.where(["lower(username) = ? or lower(email) = ?",username.downcase,username.downcase]).first
		else
			user = User.where(["facebook_id = ?",facebook_id]).first
		end
		user = user.present? ? user : User.new
		if user.persisted?
			user.authenticated = user.password.eql?(Digest::SHA1.hexdigest(password + user.salt))
		end
		
		if user.authenticated
			user.update_attribute(:authentication_token , User.digest(Time.now.to_s) ) if user.authentication_token.blank?
		else
			user.errors.add("in-valid","username or password")
		end
		user
		
	end

	def self.digest(value)
		salt = Digest::SHA1.hexdigest(rand(1234...9045369).to_s)
		Digest::SHA1.hexdigest(value+salt)
	end
	
	def pass_gen
		self.salt = Digest::SHA1.hexdigest(rand(1234...9045369).to_s)
		self.password = Digest::SHA1.hexdigest(self.password + self.salt)
  end

  def as_json(options = { })
    json = super(options)
    json[:password_confirmation] = self.password_confirmation
    json
  end

end
