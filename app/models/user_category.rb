class UserCategory < ActiveRecord::Base
	belongs_to :category
	belongs_to :user
	validates_uniqueness_of :category ,:scope => :user
end
