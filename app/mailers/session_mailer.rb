class SessionMailer < ActionMailer::Base
  default from: YAML.load_file('config/api.yml')[Rails.env]["default_mail_from"]

 def reset_password_email(user)
    @user = user
    @from =  YAML.load_file('config/api.yml')[Rails.env]["default_mail_from"]
    @url  = "#{YAML.load_file('config/api.yml')[Rails.env]["domain"]}api/reset_password/#{@user.reset_token}"
    mail(to: @user.email, subject: 'Reset Password Request')
  end


  def recover_username_email(user)
    @user = user
    @from =  YAML.load_file('config/api.yml')[Rails.env]["default_mail_from"]
    mail(to: @user.email, subject: 'Account Request')
  end
end
