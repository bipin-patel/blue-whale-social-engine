class CategoriesController < ApiController
	before_filter :validate_auth_token

	def index
		cats = Category.where(:object_type => params[:object_type])
		render :json => {:categories => cats}
	end
end
