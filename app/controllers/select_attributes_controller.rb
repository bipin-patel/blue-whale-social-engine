class SelectAttributesController < ApplicationController

  def get_options_by_name
    options = SelectAttribute.where(name: params[:name]).first.options
    render json: options
  end


end
