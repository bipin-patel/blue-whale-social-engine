class PostCommentsController < ApiController
	before_filter :validate_auth_token

	def index
		params[:page] ||= 1
		params[:per] ||= 25
		params[:sort] ||= "comments.created_at desc"
		@comments = Comment.joins(:user).where(:object_type_id => params[:post_id],:object_type => "post").order(params[:sort]).page(params[:page]).per(params[:per])
	end

	def show
		@comment = Comment.find(params[:id])
	end

	def create
		@comment = Comment.new(comment_params)
		if @comment.valid?
			@comment.user_id = current_user.id
			@comment.save
			render :show
		else
			handle_api_error(@comment)
		end
	end

	def update
		@comment = Comment.find(params[:id])
		if @comment.update_attributes(comment_params)
			render :show
		else
			handle_api_error(@comment)
		end
	end

	def destroy
			@comment = Comment.find(params[:id])
			@comment.update_attribute("deleted",true)
			render :json => {:result => "Comment deleted"}
	end

	private
	def comment_params
		params.require(:comment).permit(:object_type_id,:object_type,:user_id,:comment,:parent_comment_id)
	end
end
