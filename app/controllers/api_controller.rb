class ApiController < ApplicationController
	skip_before_filter :verify_authenticity_token
	before_filter :validate_api_key, :except => [:reset_password,:change_password]
	
	attr_accessor :errors,:current_user

	#custom error handler
	# if !Rails.env.test?
	# 	rescue_from StandardError do |exception|
	#
	# 			if Rails.env.development?
	# 				logger = Logger.new(STDOUT)
	# 				logger.debug("\n\n" + exception.message + "\n\n" +exception.backtrace.join("\n"))
	# 			end
	# 			@errors = [exception.message]
	# 		   	render :partial => "shared/api_error",:status => 500
	# 	end
	# end

	def routing_error
		handle_api_error("In-valid api function or route")
	end

	def validate_auth_token
		if cookies[:authentication_token].blank?
			return handle_api_error('You must log in to perform this function')
		else
		 	user = User.where("authentication_token = ?",cookies[:authentication_token]).first
		 	if user.blank?
		 		return handle_api_error('You must log in to perform this function')
		 	else
		 		self.current_user = user
		 	end
		end
	end

	def handle_api_error(obj)
		@errors = obj.is_a?(String) ? [obj] : obj.errors.full_messages
		render :partial => "shared/api_error",:status => 400
		return
	end

	def validate_api_key
		#return if Rails.env.development?
		self.errors = []
		# logger = Logger.new(STDOUT)
		# logger.debug(request.headers['HTTP_X_API_TOKEN'])
		user = User.new
		if !YAML.load_file('config/api.yml')[Rails.env]["api_key"]
				user.errors.add("api","config file missing. Did you add api.yml to your config?")
				handle_api_error(user)
		else 
			
			if cookies[:api_key].blank?
				user.errors.add("api","key was not sent")
				handle_api_error(user)
			else
				if !cookies[:api_key].eql?(YAML.load_file('config/api.yml')[Rails.env]["api_key"])
					user.errors.add("api","key is in-valid")
					handle_api_error(user)
				end
			end

		end
	end


	
	def build_condition(cond)
		cond_str = []
		cond_arry = []
		cond.each do |k,v|
			cond_str << "#{k} = ?"
		end
		cond_arry << cond_str.join(" and ")
		cond.map{|k,v|  cond_arry << v }
		return cond_arry
	end

end

