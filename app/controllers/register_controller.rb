class RegisterController < ApiController


	def create
		return handle_api_error('User data not sent') if params[:user].blank?
		#if registration_params[:avatar].present?
	#		registration_params[:avatar] = User.decode_avatar(registration_params[:avatar])
#		end
		@user = User.new(registration_params)
		if @user.valid?
			@user.authentication_token = User.digest(Time.now.to_s)
      @user.save
      self.current_user = @user
      render :create
		else 
			handle_api_error(@user)
		end
	end


	private
    def registration_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation,:first_name, :last_name,:avatar,
      	:facebook_id,:twitter_id,:oauth_token,:company_name,:is_business,:birthdate,
      	:address1,:address2,:city, :state,:zip, :phone,:country,:avatar,:gender,:device_id,:categories, meta_data: params[:user][:meta_data].try(:keys)
      	)
    end
end
