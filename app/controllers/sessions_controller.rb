class SessionsController < ApiController
	before_filter :validate_auth_token, :except => [:create,:reset_password,:change_password,:forgot_password,:forgot_username]

	def create
		return handle_api_error('In-valid username or password') if params[:user].blank?

		# check for facebook_id, if sent, then try to login with the id, otherwise create an account for it.
		if params[:user][:facebook_id].present?
			@user = User.authenticate_with_facebook(registration_params)
			return handle_api_error('In-valid username or password') if @user.nil?
			if @user.authenticated
				self.current_user = @user
				#@user.auto_create_facebook_friends
				render :create 
			else
				return handle_api_error(@user)	
			end
			return
		end

		return handle_api_error('In-valid username or password') if params[:user][:password].blank?
		if params[:user][:username].present?

			@user = User.authenticate(params[:user][:username],params[:user][:password])
		elsif params[:user][:email].present?
			@user = User.authenticate(params[:user][:email],params[:user][:password])
		else
			return handle_api_error('In-valid username or password')
		end
		 
		if @user.authenticated 
			self.current_user = @user
			render :create
		else
			return handle_api_error(@user)
		end
	end

	def forgot_username
		return handle_api_error('In-valid email') if params[:user].blank?
		@user = User.where(["lower(email) = :email",{:email => params[:user][:email].downcase} ] ).first
		if @user.present?
			SessionMailer.recover_username_email(@user).deliver
		else
			return handle_api_error('Email not found') 
		end
	end

	def forgot_password
		return handle_api_error('In-valid username or email') if params[:user].blank?
		user_lookup = params[:user][:email].present? ? params[:user][:email] : params[:user][:username]
		@user = User.where(["lower(username) = :user or lower(email) = :user",{:user => user_lookup.downcase} ] ).first
		if @user.present?
			@user.update_attribute("reset_token",User.digest(Time.now.to_s) )
			SessionMailer.reset_password_email(@user).deliver
		else
			return handle_api_error('Username or email not found') 
		end
	end

	def reset_password
		@user = User.where(["reset_token = ?",params[:reset_token]]).first
		raise ActionController::RoutingError.new('Not Found') if @user.blank?
	end

	def change_password
		@user = User.where(["reset_token = ?",params[:reset_token]]).first
		raise ActionController::RoutingError.new('Not Found') if @user.blank?
		 
		if @user.update_attributes(change_pass_params)
			@user.pass_gen 
			@user.reset_token = nil
			@user.save!(:validate => false)
			render :change_password
		else
			flash[:errors] = @user.errors.full_messages
			render :reset_password
		end
	end

	def destroy
		current_user.update_attribute("authentication_token",nil)
	end

	private
    def change_pass_params
      params.require(:user).permit(:password,:password_confirmation)
      	
    end
    def registration_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation,:first_name, :last_name,:avatar,
      	:facebook_id,:twitter_id,:oauth_token,:company_name,:is_business,:birthdate,
      	:address1,:address2,:city, :state,:zip, :phone,:country,:avatar,:gender,:device_id,:categories
      	)
    end
end
