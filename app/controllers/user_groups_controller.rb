class UserGroupsController < ApiController

	before_filter :validate_auth_token

	def index
		params[:page] ||= 1
		params[:per] ||= 25
		@user_groups = Group.where(:object_owner_id => current_user.id).page(params[:page]).per(params[:per])
	end


	def create
		@user_group = Group.new(group_params)
		@user_group.users << User.where(:id => params[:group][:users])
		if @user_group.valid?
			@user_group.object_owner_id = current_user.id
			@user_group.save
		else
			handle_api_error(@user_group)
		end
	end

	def show
		@user_group = Group.find(params[:id])
	end

	def update
		@user_group = Group.find(params[:id])
		@user_group.update_attributes(group_params)
		render :show
	end

	def destroy
		@user_group = Group.where(:id => params[:id],:object_owner_id => current_user.id).first
		@user_group.destroy
		index
		render :index
	end


	private
    def group_params
      params.require(:group).permit(:name,:users)
    end
end
