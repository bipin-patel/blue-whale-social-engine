class FollowersController < ApiController
	before_filter :validate_auth_token

	def index
		params[:page] ||= 1
		params[:per] ||= 25
		@followers = Follower.joins(:user).where(:following_id => current_user.id).page(params[:page]).per(params[:per])
	end

	def show
		@follower = Follower.find(params[:id])
	end

	def create
		@follower = Follower.new(follower_params)
		@follower.user_id = current_user.id
		if @follower.valid?
			@follower.save
		else
			handle_api_error(@follower)
		end
	end

	def update
		@follower = Follower.where(:following_id => params[:id],:user_id => current_user.id).first
		if @follower.update_attributes(follower_params)
			render :show
		else
			handle_api_error(@follower)
		end
	end

	def destroy
		@follower = Follower.where(:following_id => params[:id],:user_id => current_user.id).first
		@follower.update_attribute("blocked",true) if @follower.present?
		@follower = Follower.where(:user_id => params[:id],:following_id => current_user.id).first
		@follower.update_attribute("blocked",true) if @follower.present?
		render :json => {:result => "Follower deleted"}
	end

	def followings
		params[:page] ||= 1
		params[:per] ||= 25
		@followings = Follower.joins(:followed_user).where(:user_id => params[:follower_id]).page(params[:page]).per(params[:per])
	end

	def pending
		@pendings = current_user.pending_invites
		 
	end

	private
	def follower_params
		params.require(:follower).permit(:following_id,:blocked)
	end
end
