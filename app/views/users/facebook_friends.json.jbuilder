json.array!(@users) do |u|
	json.name u[:user]["name"]
	json.id u[:user]["id"]
	json.is_member u[:user]["is_member"]
	json.is_following u[:user]["is_following"]
	json.is_invited u[:user]["is_invited"]
end