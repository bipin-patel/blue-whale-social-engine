json.group do 
	json.name user_group.name
	json.id user_group.id
	json.users do
		json.array!(user_group.users) do |u|
				json.partial! 'shared/user', {:user_instance => u}
		end
	end
end