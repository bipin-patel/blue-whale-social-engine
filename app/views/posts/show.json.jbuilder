 json.partial! 'shared/post', {:post => @post }
	json.subscribers do 
		json.partial! 'subscribers', {:subscribers => @post.subscribers}
	end
	json.assets do 
		json.partial! 'assets', {:assets => @post.assets}
	end
 	json.children do
	  	json.array!(@post.children) do |c|
	 		json.partial! 'shared/post', {:post => c }
			json.assets do 
				json.partial! 'assets', {:assets => c.assets}
			end
	 	end

 	end
 


	
 