
json.array!(assets) do |a|
	if a.is_photo?
	 json.large2x a.file(:large2x)
	 json.large a.file(:large)
	 json.thumb2x a.file(:thumb2x)
	 json.thumb a.file(:thumb)
	 json.original a.file(:original)
	 else 
	 json.video a.file
 	end
end