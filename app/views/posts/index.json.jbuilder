json.total_rows  @posts.total_count
 
json.posts do 
json.array!(@posts) do |p|
	
	json.partial! 'shared/post', {:post => p }
	json.subscribers do 
		json.partial! 'posts/subscribers', {:subscribers => p.subscribers}
	end
	json.assets do 
		json.partial! 'posts/assets', {:assets => p.assets}
	end
 	json.children do
	  	json.array!(p.children) do |c|
	 		json.partial! 'shared/post', {:post => c }
			json.assets do 
				json.partial! 'posts/assets', {:assets => c.assets}
			end
	 	end

 	end
 
end

end