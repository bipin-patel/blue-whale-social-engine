class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
    	t.integer :user_id
    	t.integer :object_id
    	t.string :object_type
      t.timestamps
    end

      add_index :likes,:user_id
  add_index :likes,:object_id
  end


end
