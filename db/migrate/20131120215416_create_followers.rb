class CreateFollowers < ActiveRecord::Migration
  def change
    create_table :followers do |t|
      t.integer :user_id
      t.integer :following_id
      t.boolean  :blocked,      :default => false
      t.timestamps
    end

    add_index :followers, :user_id
    add_index :followers,:following_id
  end
end
