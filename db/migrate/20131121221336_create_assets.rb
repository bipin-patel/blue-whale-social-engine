class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
    	t.string :object_type
    	t.string :object_id
      t.timestamps
    end

    add_attachment :assets, :file
  end
end
