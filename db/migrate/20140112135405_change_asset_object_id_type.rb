class ChangeAssetObjectIdType < ActiveRecord::Migration
  def change
  	remove_column :assets, :object_id
  	add_column :assets, :object_id ,:integer
  end
end
