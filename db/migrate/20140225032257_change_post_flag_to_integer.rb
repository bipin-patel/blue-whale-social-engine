class ChangePostFlagToInteger < ActiveRecord::Migration
  def change
  	remove_column :posts, :flag
  	add_column :posts, :flag, :integer
  end
end
