class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
    	t.string :name
    	t.integer :object_type_id
    	t.string :object_type, :default => "post"
      t.timestamps
    end
  end
end
