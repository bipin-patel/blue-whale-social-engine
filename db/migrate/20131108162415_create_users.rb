class CreateUsers < ActiveRecord::Migration
  def change
    execute "CREATE EXTENSION IF NOT EXISTS hstore"

    create_table :users do |t|
      t.string :email
      t.string :username
      t.string :password
      t.string :salt
      t.string :facebook_id
      t.string :twitter_id
      t.string :first_name
      t.string :last_name
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :country
      t.date :birthdate
      t.string :zip
      t.string :phone
      t.string :website
      t.text :description
      t.string :company_name
      t.boolean :is_business, :default => false
      t.boolean :is_deleted, :default => false
      t.string :oauth_token
      t.string :authentication_token
      t.string :device_id
      t.hstore :meta_data
      t.timestamps
    end

    add_attachment :users, :avatar

    add_index :users, :username, :unique => true
    add_index :users, :email, :unique => true
    add_index :users, :facebook_id, :unique => true


  end
end
