# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140408004328) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "assets", force: true do |t|
    t.string   "object_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "object_type_id"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "object_type_id"
    t.string   "object_type",    default: "post"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.integer  "object_type_id"
    t.string   "object_type",       default: "post"
    t.integer  "user_id"
    t.integer  "parent_comment_id"
    t.text     "comment"
    t.boolean  "deleted",           default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "favorites", force: true do |t|
    t.integer  "user_id"
    t.integer  "object_type_id"
    t.string   "object_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "followers", force: true do |t|
    t.integer  "user_id"
    t.integer  "following_id"
    t.boolean  "blocked",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "followers", ["following_id"], name: "index_followers_on_following_id", using: :btree
  add_index "followers", ["user_id"], name: "index_followers_on_user_id", using: :btree

  create_table "groups", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "object_owner_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invites", force: true do |t|
    t.integer  "user_id"
    t.string   "invited_email"
    t.string   "invited_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "invited_user_id"
  end

  create_table "likes", force: true do |t|
    t.integer  "user_id"
    t.integer  "object_type_id"
    t.string   "object_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "likes", ["object_type_id"], name: "index_likes_on_object_type_id", using: :btree
  add_index "likes", ["user_id"], name: "index_likes_on_user_id", using: :btree

  create_table "post_categories", force: true do |t|
    t.integer  "category_id"
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
  end

  create_table "post_subscribers", force: true do |t|
    t.integer "post_id"
    t.integer "user_id"
    t.integer "group_id"
    t.boolean "blocked",  default: false
  end

  create_table "posts", force: true do |t|
    t.integer  "parent_post_id"
    t.integer  "user_id"
    t.text     "post_text"
    t.boolean  "public",         default: true
    t.boolean  "is_deleted",     default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "location"
    t.string   "zip"
    t.integer  "flag"
    t.integer  "views",          default: 0
    t.string   "post_title"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.boolean  "is_admin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "select_attributes", force: true do |t|
    t.string   "name"
    t.hstore   "options"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "select_attributes", ["name"], name: "index_select_attributes_on_name", unique: true, using: :btree

  create_table "user_categories", force: true do |t|
    t.integer  "category_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_groups", force: true do |t|
    t.integer  "group_id"
    t.integer  "user_id"
    t.boolean  "blocked",    default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "username"
    t.string   "password"
    t.string   "salt"
    t.string   "facebook_id"
    t.string   "twitter_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.date     "birthdate"
    t.string   "zip"
    t.string   "phone"
    t.string   "website"
    t.text     "description"
    t.string   "company_name"
    t.boolean  "is_business",          default: false
    t.boolean  "is_deleted",           default: false
    t.string   "oauth_token"
    t.string   "authentication_token"
    t.string   "device_id"
    t.hstore   "meta_data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "reset_token"
    t.string   "gender"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["facebook_id"], name: "index_users_on_facebook_id", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "users_roles", force: true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
